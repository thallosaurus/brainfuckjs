//Simple Brainfuck interpreter
import fs from "fs";

//redeclare Array Class for easy switch during development
const BFCell = Uint8Array;

export default class Brainfuck {

    /**
     * Beschreibt die Position der Zelle
     */
    ptr;

    /**
     * Beschreibt die aktuelle Position im Programmcode
     */
    prgPtr;

    /**
    * Speichert die Adressen, welche bei Schleifen zwischengespeichert werden
    */
    stack;

    /**
     * Speicherzelle, welche vom Programm verändert wird
     */
    cell;

    /**
     * Variable, welche das geladene Programm speichert
     */
    program;

    /**
     * Repräsentiert die Opcode-Tabelle
     * 
     * | Zeichen | C-Äquivalent | Semantik |
     * | --- | --- | --- |
     * | > | ```++ptr;``` | Inkrementiert den Zeiger |
     * | < | ```--ptr;``` | Dekrementiert den Zeiger |
     * | + | ```++*ptr;``` | inkrementiert den aktuellen Zellenwert |
     * | - | ```--*ptr;``` | dektementiert den aktuellen Zellenwert |
     * | . | ```putchar(*ptr);``` | Gibt den aktuellen Zellenwert als ASCII-Zeichen auf der Standardausgabe aus |
     * | , | ```*ptr = getchar();``` | Liest ein Zeichen von der Standardeingabe und speichert dessen ASCII-Wert in der aktuellen Zelle |
     * | [ | ```while (*ptr) {``` | Springt nach vorne, hinter den passenden ```]```-Befehl, wenn der aktuelle Zellenwert 0 ist |
     * | ] | ```}``` | Springt zurück, hinter den passenden ```[```-Befehl, wenn der aktuelle Zellenwert nicht 0 ist |
     * 
     * (Quelle: [Brainfuck - Wikipedia](https://de.wikipedia.org/wiki/Brainfuck))
     */
    opcodes = {
        ">": () => {
            if (this.ptr++ === this.cell.length) {
                this.ptr = 0;
            }
        },
        "<": () => {
            if (this.ptr-- === -1) {
                this.ptr += this.cell.length
            }
        },
        "+": () => this.cell[this.ptr]++,
        "-": () => this.cell[this.ptr]--,
        ".": () => { this.stdout(this.cell[this.ptr]) },
        ",": () => { this.cell[this.ptr] = (this.stdin() % 256) },
        "[": () => {
            if (this.cell[this.ptr] === 0) {
                this.prgPtr += this.fwd();
            } else {
                this.stack.push(this.prgPtr);
            }
        },
        "]": () => {
            this.prgPtr = (this.stack.pop() - 1);
        }
    }

    /**
     * Legt fest, ob das Skript direkt auf die Konsole schreiben kann oder nicht (wie z. B. in Visual Studio Code der Fall bei Verwendung der Debug Console)
     */
    useTTYBufferOutput = !process.stdout.isTTY;

    /**
     * Sollte ein Konsolenzugriff nicht möglich sein, so wird der Output in diesem Buffer gespeichert und am Ende ausgegeben
     */
    ttyBuffer = "";

    constructor(prog = "") {
        this.reset();
        
        this.program = this.removeComments(prog);

        this.run();
    }

    /**
     * Setzt die Klasse zurück auf Standartwert
     */
    reset() {
        this.ptr = 0;
        this.prgPtr = -1;
        this.cell = new BFCell(30000 /* 2 ** 15 */);
        this.stack = new Array();
    }

    /**
     * Führt das Brainfuck Programm aus
     */
    run() {
        this.prgPtr = 0;
        while (this.prgPtr !== -1) {
            let instruction = this.program[this.prgPtr];

            if (Object.keys(this.opcodes).indexOf(instruction) != -1) {
                this.opcodes[instruction]();
            }

            this.prgPtr++;

            if (this.prgPtr == this.program.length) this.prgPtr = -1;
        }
        if (this.useTTYBufferOutput) console.log(this.ttyBuffer);
    }

    /**
     * Spult vor bis zum nächsten ```]``` und setzt ```ptr``` auf die neue Adresse
     */
    fwd() {
        let substring = this.program.substring(this.prgPtr);
        let count = 0;
        let ipointer = 0;
        while (ipointer < substring.length) {
            ipointer++;
            if (substring[ipointer] === "[") {
                count++;
                // console.log("Found nested loop at", ipointer);
            }
            else if (substring[ipointer] === "]") {
                if (count) {
                    count--;
                } else {
                    break;
                }
            }
        }

        return ipointer;
    }

    /**
     * Gibt den übergebenen ASCII-Code encodiert auf der STDOUT aus
     */
    stdout(key) {
        if (process.stdout.isTTY) {
            process.stdout.write(String.fromCharCode(key));
        } else {
            this.ttyBuffer += String.fromCharCode(key);
        }
    }

    /**
     * Liest ein Zeichen von STDIN ein
     */
    stdin() {
        if (process.stdin.isTTY) {
            let buffer = Buffer.alloc(1);
            fs.readSync(0, buffer, 0, 1);
            return buffer.toString('utf8').charCodeAt();
        } else {
            return 0x41;    //Using ASCII A
        }
    }

    /**
     * Hilfsfunktion um den Interpreter mittels Datei zu starten
     */
    static file(filename) {
        if (!filename) {
            throw new Error("No file path given");
        }

        let data = "";
        try {
            data = fs.readFileSync(filename).toString();
        } catch (e) {
            console.error(e);
        }

        if (data) {
            return new Brainfuck(data);
        }
    }

    /**
     * Entfernt jedes nicht-Brainfuck Zeichen vom Programmcode
     */
    removeComments(program) {
        let buf = "";
        for (let i = 0; i < program.length; i++) {
            if (Object.keys(this.opcodes).includes(program[i])) {
                buf += program[i];
            }
        }

        return buf;
    }
}