import fs from 'fs';
import path from 'path';

if (!process.argv[2]) {
    throw new Error("No file given - bye");
}

let debug = false;

let bf = fs.readFileSync(process.argv[2]).toString();   //test prog
let indentation = 0;
let buffer = `
/*
ORIGINAL FILE:
${bf}*/
let cPointer = 0;
let cells = new Uint8Array(2**15);
let stdout = "";

`;

for (let i = 0; i < bf.length; i++) {
    //add indentation
    for (let i = 0; i < indentation; i++) {
        buffer += "    ";
    }

    let newL = true;

    switch(bf[i]) {
        case ">":
            buffer += "cPointer++; //>";
            break;

        case "<":
            buffer += "cPointer--; //<";
            break;

        case "+":
            buffer += "cells[cPointer]++; //+";
            break;

        case "-":
            buffer += "cells[cPointer]--; //-"
            break;

        case ".":
            buffer += "stdout += (String.fromCharCode(cells[cPointer])); //.";
            break;

        case ",":
            buffer += "//reserved for input";
            break;

        case "[":
            buffer += "while(cells[cPointer] !== 0){ //["
            indentation++;
            break;

        case "]":
            buffer += "} //]";
            indentation--;
            break;

        default:
            newL = false;
    }

    newL && (buffer += "\r\n");

    if (debug) {
        buffer += `console.log("Cells", cells);
        console.log("cPointer: ", cPointer);
        console.log("stdout: ", stdout);
        debugger;`
    }
}

buffer += "console.log(stdout);";

// console.log(buffer);
fs.writeFileSync("./out/" + path.basename(process.argv[2]) + ".js", buffer);